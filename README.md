# Mail to wallet

My python script which receive mails with scan (from epson connect) and transfers the attachments to a cloud storage.

## Prepare

Set up configuration in the file .dotenv

## Run it

Ensure to have the environment variable set up :

```shell
OUTPUT_FOLDER
```

Using python3, install the required packages (requirements.txt) and run it.

```shell
python3 ./mail-to-wallet.py
```

## Advanced

The python program will connect to the pop-server and search for new mails.

After being processed, the mail is deleted.

It is a good idea to create a cronjob to run the script every 2 minutes.
