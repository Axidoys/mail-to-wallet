import os
import poplib
from email import message_from_string
from dotenv import load_dotenv

load_dotenv()

EMAIL_ADDR = os.getenv("MTW_MAIL")
EMAIL_PASSWD = os.getenv("MTW_PASSWORD")
SERVER_ADDR, SERVER_PORT = os.getenv("MTW_POP_SERVER").split(":")

DROP_FOLDER = os.getenv("MTW_DROP_FOLDER")

EXPECTED_AUTHOR = os.getenv("MTW_EXPECTED_AUTHOR")
EXPECTED_SUBJECT = os.getenv("MTW_EXPECTED_SUBJECT")


def connect_to_pop_server():
    client = poplib.POP3_SSL(SERVER_ADDR, port=int(SERVER_PORT))
    welcome_msg = client.getwelcome().decode("utf-8")
    print(f"welcome : {welcome_msg}")
    client.user(EMAIL_ADDR)
    client.pass_(EMAIL_PASSWD)
    return client


def download_attachment(client, i):
    resp, mail_raw, octets = client.retr(i)
    raw_email_strings = [mail.decode('utf-8') for mail in mail_raw]
    email_message = message_from_string("\n".join(raw_email_strings))
    # downloading attachments
    for part in email_message.walk():
        if part.get_content_maintype() == 'multipart':
            continue
        if part.get('Content-Disposition') is None:
            continue
        file_name = part.get_filename()
        if bool(file_name):
            file_path = os.path.join(DROP_FOLDER, file_name)
            if not os.path.isfile(file_path):
                fp = open(file_path, 'wb')
                fp.write(part.get_payload(decode=True))
                fp.close()
            else:
                print("Error ! Already downloaded file !")
                return False

    return True


def is_mail_concerned(client, i):
    resp, mail_raw, octets = client.top(i, 20)
    raw_email_strings = [mail.decode('utf-8') for mail in mail_raw]
    email_message = message_from_string("\r\n".join(raw_email_strings))

    subject = email_message.get("Subject")
    author = email_message.get("From")

    return author == EXPECTED_AUTHOR and subject == EXPECTED_SUBJECT


def main():
    print("Connecting to the pop-server")
    client = connect_to_pop_server()

    print(f"Stats (n-msg, size) : {client.stat()}")

    resp, mails, octets = client.list()

    mail_index_range = [int(l.decode("utf-8").split(" ")[0]) for l in mails]
    mail_index_range.sort(reverse=True)
    for i in mail_index_range:
        print(i)
        if is_mail_concerned(client, i):
            if download_attachment(client, i):
                client.dele(i)
    client.quit()

# TODO * add time of processing logging/warning

if __name__ == "__main__":
    main()
